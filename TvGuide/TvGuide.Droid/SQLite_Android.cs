using System;
using TvGuide;
using Xamarin.Forms;
using System.IO;

using TvGuide.Droid;
using TvGuide.SqlLite;



[assembly: Dependency(typeof(SQLite_Android))]

namespace TvGuide.Droid
{
// ...
    public class SQLite_Android : ISQLite
    {
        public SQLite_Android()
        {
            
        }
        #region ISQLite implementation

        public SQLite.SQLiteConnection GetConnection()
        {
            var sqliteFilename = "TvSQLite.db3";
            string documentsPatch = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string path = Path.Combine(documentsPatch, sqliteFilename);

            Console.WriteLine(path);
            var conn = new SQLite.SQLiteConnection(path);

            return conn;
        }       
            
        #endregion

    }
}