using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TvGuide.Custom_controls;
using TvGuide.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Button = Xamarin.Forms.Button;

[assembly: ExportRenderer(typeof(SpeakerButton), typeof(SpeakerButtonRenderer))]

namespace TvGuide.Droid
{
    class SpeakerButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.speaker));
            }
        }
    }
}