﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nito.AsyncEx;
using TvGuide.Services.Models;
using TvGuide.Views;
using Xamarin;
using Xamarin.Forms;

namespace TvGuide
{
    public class App : Application
    {

       
        
        public App()
        {

            //List<Channel> channels = AsyncContext.Run(() => tvServices.GetAllChannels());
           
            // The root page of your application
            MainPage = new NavigationPage(new ChannelListPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
