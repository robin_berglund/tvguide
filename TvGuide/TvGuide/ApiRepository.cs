﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using TvGuide.Services.Models;

namespace TvGuide
{
    public class ApiRepository
    {
        public async Task<XDocument> GetAllChannelsXdoc()
        {
            string requestUrl = "http://xmltv.tvsajten.com/xmltv/channels.xml.gz";
            XDocument xDoc;
            HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
            request.ContentType = "application/xml";
            request.Method = "GET";
            try
            {
                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        GZipStream zip = new GZipStream(stream, CompressionMode.Decompress);
                        xDoc = XDocument.Load(zip);
                        // xDoc.Declaration = new XDeclaration("1.0", "utf-8", null);
                        return xDoc;
                    }
                }
            }
            catch (Exception)
            {
                throw new Exception("Tjänsten kunde inte nås. Försök igen senare.");
            }
        }

        public async Task<XDocument> GetProgramsXdocForChannel(string channelUrl)
        {
            XDocument xDoc = new XDocument();
            HttpWebRequest request = WebRequest.Create(channelUrl) as HttpWebRequest;
            request.ContentType = "application/xml";
            request.Method = "GET";

            try
            {
                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        GZipStream zip = new GZipStream(stream, CompressionMode.Decompress);
                        xDoc = XDocument.Load(zip);
                        // xDoc.Declaration = new XDeclaration("1.0", "utf-8", null);
                        return xDoc;
                    }
                }
            }
            catch (Exception)
            {
                throw new Exception("Kanalen kunde inte hittas.");
            }
        }
    }
}