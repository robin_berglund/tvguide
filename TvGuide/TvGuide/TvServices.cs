﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using TvGuide.Services.Models;

namespace TvGuide
{
    public class TvServices
    {
        
        public static async Task<List<Channel>> GetAllChannels()
        {
            await Task.Delay(1000);
            ApiRepository rep = new ApiRepository();
            XDocument doc;
            try
            {
                doc = await rep.GetAllChannelsXdoc();
            }
            catch (Exception ex)
            {
                throw ex;
            }
               
                List<Channel> channels = new List<Channel>();
                List<string> list = new List<string>();
                foreach (XElement e in doc.Root.Elements())
                {
                Channel c = new Channel();
                    foreach (XElement e2 in e.Descendants())
                    {
                        if (e2.Name == "display-name")
                        {
                            c.ChannelName = e2.Value;
                        }
                        if (e2.Name == "base-url")
                        {
                            c.Url = e2.Value + e.FirstAttribute.Value;
                        }
                       
                    }
                    
                channels.Add(c);
                }

                return channels;
        
        }

        public static async Task<Channel> PopulateProgramsForChannel(Channel channel, DateTime date)
        {
            ApiRepository rep = new ApiRepository();
          
            string formatDate = date.ToString("yyyy-MM-dd");
            string url = channel.Url + "_" + formatDate + ".xml.gz";
            XDocument doc;
            try
            {
                try
                {
                    doc = await rep.GetProgramsXdocForChannel(url);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                
                if (doc.Root.Elements().Count() <= 1)
                {
                    throw new Exception("Ingen info kunde hittas för kanalen.");
                }
                foreach (XElement e in doc.Root.Elements())
                {
                    string[] dateSeparated = e.FirstAttribute.Value.Split(' ');
                    Program p = new Program();
                    try
                    {
                        p.StartTime = DateTime.ParseExact(dateSeparated[0], "yyyyMMddHHmmss",
                        null);
                    }
                    catch (Exception)
                    {
                        p.StartTime = new DateTime();
                    }
                    foreach (XElement e2 in e.Elements())
                    {
                        if (e2.Name == "title")
                        {
                            p.Title = e2.Value;
                        }
                        if (e2.Name == "desc")
                        {
                            p.Description = e2.Value;
                        }
                        if (e2.Name == "date")
                        {
                            p.ReleaseYear = int.Parse(e2.Value);
                        }
                        if (e2.Name == "category")
                        {
                            if (p.Category != string.Empty)
                            {
                                p.Category += ", " + e2.Value;
                            }
                            else
                            {
                                p.Category = e2.Value;
                            }
                            
                        }
                    }
                    channel.Programs.Add(p);
                }
                if (channel.Programs.Count() <1)
                {
                    Program program = new Program { Title = "Hittade inga program för kanalen"};
                    channel.Programs.Add(program);
                }
                return channel;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            
        }
        
    }
}
