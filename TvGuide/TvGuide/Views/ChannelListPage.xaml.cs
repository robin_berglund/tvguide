﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TvGuide.Services.Models;
using TvGuide.Views.ViewModels;
using Xamarin.Forms;

namespace TvGuide.Views
{
    public partial class ChannelListPage : ContentPage
    {
        private ChannelListViewModel model;

        public ChannelListPage()
        {

            model = new ChannelListViewModel();
            BindingContext = model;
            
            InitializeComponent();
       
          
            
        }
        private void ButtonGoToChannelClicked(Object sender, EventArgs EventArgs)
        {
           
            var button = (Button) sender;
            if (button.CommandParameter != null && model.Channels.IsSuccessfullyCompleted)
            {
                Channel channel = model.Channels.Result.FirstOrDefault(c => c.Url == (string) button.CommandParameter);
                Navigation.PushAsync(new ChannelPage(channel, DateTime.Now));
            }
            
        }
        private void GoToFavorites(Object sender, EventArgs EventArgs)
        {
                Navigation.PushAsync(new Favorites());
        }


    }
}
