﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nito.AsyncEx;
using TvGuide.Resources;
using TvGuide.Services.Models;
using TvGuide.SqlLite;
using TvGuide.ViewModels;
using Xamarin.Forms;

namespace TvGuide.Views
{
    public partial class ChannelPage : ContentPage
    {
        private ChannelViewModel model;
        public ChannelPage(Channel channel, DateTime date)
        {
            model = new ChannelViewModel(channel, date);
            BindingContext = model;
            //ToolbarItems.Add(new ToolbarItem
            //{
            //    Order = ToolbarItemOrder.Primary,
            //    Icon = "plus.png"
            //});
            InitializeComponent();
       
        }
        private void AddFavorite(Object sender, EventArgs EventArgs)
        {
            ChannelDatabase db = new ChannelDatabase();
            if (model.Channel.IsSuccessfullyCompleted)
            {
                LightChannel channel;
                Channel c1 = model.Channel.Result;
                if (db.GetItems().FirstOrDefault(c => c.ChannelName == c1.ChannelName) == null)
                {
                   channel = new LightChannel(); 
                   channel.ChannelName = c1.ChannelName;
                   channel.Url = c1.Url;
             
                db.SaveItem(channel);   
                }
              
            }

        }


        private void TtoS(object sender, EventArgs e)
        {
            var button = (Button) sender;
            var tToS = DependencyService.Get<ITextToSpeech>();

            tToS.Speak("Hello");
        }
    }
}
