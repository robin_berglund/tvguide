﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using TvGuide.Services.Models;
using TvGuide.SqlLite;
using TvGuide.ViewModels;
using Xamarin.Forms;

namespace TvGuide.Views
{
    public partial class Favorites : ContentPage
    {
        private FavoritesListViewModel model;
        public Favorites()
        {
            model = new FavoritesListViewModel();
            BindingContext = model; 
            
            
            InitializeComponent();
        }
        private void ButtonGoToChannelClicked(Object sender, EventArgs EventArgs)
        {

            var button = (Button)sender;
            if (button.CommandParameter != null)
            {
                Channel channel = new Channel {Url = (string)button.CommandParameter};
                Navigation.PushAsync(new ChannelPage(channel, DateTime.Now));
            }

        }
        private void DeleteFavClicked(Object sender, EventArgs EventArgs)
        {
            ChannelDatabase db = new ChannelDatabase();
            var button = (Button)sender;
            if (button.CommandParameter != null)
            {
                db.DeleteItem((int) button.CommandParameter);
                LightChannel channel = model.Channels.FirstOrDefault(c => c.Id == (int) button.CommandParameter);
                model.Channels.Remove(channel);
                //Navigation.PushAsync(new Favorites());
            }

        }

    }
  
}

