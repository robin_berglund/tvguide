﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using TvGuide.Services.Models;
using Xamarin.Forms;


namespace TvGuide.SqlLite
{
    public class ChannelDatabase
    {
        static object locker = new object();
        private SQLiteConnection database;

        public ChannelDatabase()
        {
            database = DependencyService.Get<ISQLite>().GetConnection();
            database.CreateTable<LightChannel>();
        }

        public List<LightChannel> GetItems()
        {
            
            lock (locker)
            {
                List<LightChannel> list = database.Query<LightChannel>("SELECT * FROM [LightChannel]");
                return  list;
            }
        }

        public IEnumerable<LightChannel> GetItemsNotDone()
        {
            lock (locker)
            {
                return database.Query<LightChannel>("SELECT * FROM [LightChannel] WHERE [Done] = 0");
            }
        }

        public LightChannel GetItem(int id)
        {
            lock (locker)
            {
                return database.Table<LightChannel>().FirstOrDefault(x => x.Id == id);
            }
        }

        public int SaveItem(LightChannel item)
        {
            lock (locker)
            {
                if (item.Id != 0)
                {
                    database.Update(item);
                    return item.Id;
                }
                else
                {
                    return database.Insert(item);
                }
            }
        }

        public int DeleteItem(int id)
        {
            lock (locker)
            {
                return database.Delete<LightChannel>(id);
            }

        }
    }
}
