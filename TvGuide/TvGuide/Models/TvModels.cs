﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;


namespace TvGuide.Services.Models
{
  
    public class Program
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int ReleaseYear { get; set; }
        public string Language { get; set; }
        public string Category { get; set; }
        public DateTime StartTime { get; set; }

    }
    public class Channel
    {
        
        public string ChannelName { get; set; }
        public string Url { get; set; }
        public List<Program> Programs { get; set; }
        public DateTime Date { get; set; }

        public Channel()
        {
            Programs = new List<Program>();
        }
    }
    public class LightChannel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string ChannelName { get; set; }
        public string Url { get; set; }
    }
}
