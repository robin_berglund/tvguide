﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Nito.AsyncEx;
using TvGuide.Resources;
using TvGuide.Services.Models;
using Xamarin.Forms;

namespace TvGuide.Views.ViewModels
{
    class ChannelListViewModel : BindableObject
    {
       
       
        public ChannelListViewModel()
        {
            Channels = new NotifyTaskCompletion<List<Channel>>(TvServices.GetAllChannels());
        }
        public NotifyTaskCompletion<List<Channel>> Channels { get; set; }

        
        
      

    
    }
}
