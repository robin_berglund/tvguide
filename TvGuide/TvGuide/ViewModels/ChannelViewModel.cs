﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TvGuide.Resources;
using TvGuide.Services.Models;

namespace TvGuide.ViewModels
{
    public class ChannelViewModel
    {
        public ChannelViewModel(Channel channel, DateTime date)
        {
            Channel = new NotifyTaskCompletion<Channel>(TvServices.PopulateProgramsForChannel(channel, date));
        }
        public NotifyTaskCompletion<Channel> Channel { get; set; }
    }
}
