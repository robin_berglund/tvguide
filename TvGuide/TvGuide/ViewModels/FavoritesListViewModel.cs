﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using TvGuide.Resources;
using TvGuide.Services.Models;
using TvGuide.SqlLite;

namespace TvGuide.ViewModels
{
    public class FavoritesListViewModel
    {
        private ChannelDatabase channelDatabase;
        public FavoritesListViewModel()
        {
            Channels = new List<LightChannel>();
            channelDatabase = new ChannelDatabase();
            if (channelDatabase.GetItems() != null)
            {
                Channels = channelDatabase.GetItems();
            }
            
        }
        public List<LightChannel> Channels { get; set; }
    }
}
