﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Nito.AsyncEx;
using TvGuide.Services.Models;

namespace TvGuide.ConsoleApp
{
    public class Program
    {
        
        static void Main(string[] args)
        {
            AsyncContext.Run(() => MainAsync(args));
        }

        static async void MainAsync(string[] args)
        {
            try
            {
                await PrintNodes();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
            

        }

        static async Task PrintNodes()
        {
            TvServices service = new TvServices();

            List<Channel> channels = await TvServices.GetAllChannels();
            
            if (channels != null)
            {
                //foreach (Channel s in channels)
                //{
                //    Console.WriteLine(s.Url);
                //}
               await PrintUrl(channels[3]);
            }
            
            //Console.ReadLine();
        }

        static async Task PrintUrl(Channel channel)
        {
            

            DateTime date = DateTime.Now;
           
            channel =  await TvServices.PopulateProgramsForChannel(channel,date );
            Console.WriteLine(channel.ChannelName);
            foreach (Services.Models.Program program in channel.Programs)
            {
                Console.WriteLine(program.Title);
                Console.WriteLine(program.StartTime.ToString("g"));
                Console.WriteLine(program.Description);
                Console.WriteLine(program.Category);
            }
            Console.ReadLine();
        } 
     
    }
}